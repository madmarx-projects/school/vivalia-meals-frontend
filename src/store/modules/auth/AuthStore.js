import jwt from 'jsonwebtoken'

import MainStore from '@/store/Store'
import credentials from '@/utils/credentials'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    isAuthenticated: false,
    authUser: {}
  },
  mutations: {
    DO_CONNECT: (state, user) => {
      state.isAuthenticated = true
      state.authUser = user
    },
    DO_DECONNECT: (state) => {
      state.isAuthenticated = false
      state.authUser = undefined
    }
  },
  getters: {
    isAuthenticated: state => state.isAuthenticated,
    authUser: state => state.authUser
  },
  actions: {
    login (store, user) {
      MainStore.dispatch('loading', true)

      return new Promise((resolve, reject) => {
        axios.post('/auth/login', user)
          .then(res => {
            localStorage.setItem('user', JSON.stringify(res.data.user))
            localStorage.setItem('jwt', res.data.token)

            if (localStorage.getItem('jwt') !== null) {
              store.commit('DO_CONNECT', res.data.user)
              resolve(store.state.authUser)
            } else {
              reject(new Error('Échec de connexion'))
            }
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    logout (store) {
      return new Promise((resolve, reject) => {
        axios.post('/auth/logout')
          .then(res => {
            localStorage.setItem('jwt', res.data.token) // res.data.token === null
            store.commit('DO_DECONNECT')
            resolve()
          })
          .catch(err => {
            console.log(err)
            reject(new Error('Échec lors de la déconnexion'))
          })
      })
    },
    verifyToken (store) {
      return new Promise((resolve, reject) => {
        jwt.verify(localStorage.getItem('jwt'), credentials.secret, (err, decoded) => {
          if (err) {
            reject(new Error('Votre session a expiré'))
          } else {
            let user = JSON.parse(localStorage.getItem('user'))
            if (user) {
              store.commit('DO_CONNECT', user)
              resolve(user)
            } else {
              reject(new Error('Cet identifiant n\'existe pas'))
            }
          }
        })
      })
    }
  }
}
