import MainStore from '@/store/Store'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    meals: []
  },
  mutations: {
    LOAD_MEALS: (state, meals) => {
      state.meals = meals
    },
    ADD_MEAL: (state, meal) => {
      state.meals.push(meal)
    },
    REMOVE_MEAL: (state, id) => {
      state.meals = state.meals.filter(p => p._id !== id)
    },
    UPDATE_MEAL: (state, meal) => {
      state.meals.forEach(p => {
        if (p._id === meal._id) {
          p = meal
          return null
        }
      })
    }
  },
  getters: {
    meals: state => state.meals,
    count: state => state.meals.length
  },
  actions: {
    loadMeals: (store) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/meals')
          .then(res => {
            store.commit('LOAD_MEALS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    loadMealsByDate: (store) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/meals/date/' + new Date().toISOString().split('T')[0])
          .then(res => {
            store.commit('LOAD_MEALS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    addMeal: (store, meal) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/meals', meal)
          .then(res => {
            store.commit('ADD_MEAL', res.data)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    removeMeal: (store, id) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.delete('/meals/' + id)
          .then(res => {
            store.commit('REMOVE_MEAL', id)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    updateMeal: (store, meal) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/meals/' + meal._id, meal)
          .then(res => {
            store.commit('UPDATE_MEAL', meal)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    }
  }
}
