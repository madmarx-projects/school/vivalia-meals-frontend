import MainStore from '@/store/Store'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    sideMenus: []
  },
  mutations: {
    LOAD_SIDE_MENUS: (state, sideMenus) => {
      state.sideMenus = sideMenus
    },
    ADD_SIDE_MENU: (state, sideMenu) => {
      state.sideMenus.push(sideMenu)
    },
    REMOVE_SIDE_MENU: (state, id) => {
      state.sideMenus = state.sideMenus.filter(p => p._id !== id)
    },
    UPDATE_SIDE_MENU: (state, sideMenu) => {
      state.sideMenus.forEach(p => {
        if (p._id === sideMenu._id) {
          p = sideMenu
          return null
        }
      })
    }
  },
  getters: {
    sideMenus: state => state.sideMenus,
    count: state => state.sideMenus.length
  },
  actions: {
    loadSideMenus: (store) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/side-menus')
          .then(res => {
            store.commit('LOAD_SIDE_MENUS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    addSideMenu: (store, sideMenu) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/side-menus', sideMenu)
          .then(res => {
            store.commit('ADD_SIDE_MENU', res.data)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    removeSideMenu: (store, id) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.delete('/side-menus/' + id)
          .then(res => {
            store.commit('REMOVE_SIDE_MENU', id)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    updateSideMenu: (store, sideMenu) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/side-menus/' + sideMenu._id, sideMenu)
          .then(res => {
            store.commit('UPDATE_SIDE_MENU', sideMenu)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    }
  }
}
