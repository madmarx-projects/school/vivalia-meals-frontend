import MainStore from '@/store/Store'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    diets: []
  },
  mutations: {
    LOAD_DIETS: (state, diets) => {
      state.diets = diets
    },
    ADD_DIET: (state, diet) => {
      state.diets.push(diet)
    },
    REMOVE_DIET: (state, id) => {
      state.diets = state.diets.filter(p => p._id !== id)
    },
    UPDATE_DIET: (state, diet) => {
      state.diets.forEach(p => {
        if (p._id === diet._id) {
          p = diet
          return null
        }
      })
    }
  },
  getters: {
    diets: state => state.diets,
    count: state => state.diets.length
  },
  actions: {
    loadDiets: (store) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/diets')
          .then(res => {
            store.commit('LOAD_DIETS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    addDiet: (store, diet) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/diets', diet)
          .then(res => {
            store.commit('ADD_DIET', res.data)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    removeDiet: (store, id) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.delete('/diets/' + id)
          .then(res => {
            store.commit('REMOVE_DIET', id)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    updateDiet: (store, diet) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/diets/' + diet._id, diet)
          .then(res => {
            store.commit('UPDATE_DIET', diet)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    }
  }
}
