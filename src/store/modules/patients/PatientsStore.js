import MainStore from '@/store/Store'
import AuthStore from '@/store/modules/auth/AuthStore'
import axios from '@/utils/axios'

export default {
  namespaced: true,
  state: {
    patients: []
  },
  mutations: {
    LOAD_PATIENTS: (state, patients) => {
      state.patients = patients
    },
    ADD_PATIENT: (state, patient) => {
      state.patients.push(patient)
    },
    ADD_PATIENTS: (state, patients) => {
      state.patients.push(patients)
    },
    REMOVE_PATIENT: (state, id) => {
      state.patients = state.patients.filter(p => p._id !== id)
    },
    UPDATE_PATIENT: (state, patient) => {
      state.patients.forEach(p => {
        if (p._id === patient._id) {
          p = patient
          return null
        }
      })
    }
  },
  getters: {
    patients: state => state.patients,
    count: state => state.patients.length,
    countFromPavillon: (state, id) => {
      return state.patients.filter((p) => {
        if (p.pavillon !== null && p.pavillon !== undefined) {
          return p.pavillon._id === id
        } else {
          return false
        }
      }).length
    }
  },
  actions: {
    loadPatients: (store) => {
      let authUser = AuthStore.getters.authUser(AuthStore.state)

      if (authUser.pavillon === undefined || authUser.pavillon === null /* || authUser.role !== 'NURSE' */) {
        // load all Patients from all Pavillons
        MainStore.dispatch('loading', true)
        return new Promise((resolve, reject) => {
          axios.get('/patients')
            .then(res => {
              store.commit('LOAD_PATIENTS', res.data)
              resolve(res.data)
            })
            .catch(err => {
              console.error(err)
              reject(err)
            })
            .then(_ => {
              MainStore.dispatch('loading', false)
            })
        })
      } else {
        // load patients from pavillon only
        MainStore.dispatch('loading', true)
        return new Promise((resolve, reject) => {
          axios.get('/patients/pavillon/' + authUser.pavillon._id)
            .then(res => {
              store.commit('LOAD_PATIENTS', res.data)
              resolve(res.data)
            })
            .catch(err => {
              console.error(err)
              reject(err)
            })
            .then(_ => {
              MainStore.dispatch('loading', false)
            })
        })
      }
    },
    loadPatientsPavillon: (store, pavillon) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.get('/patients/pavillon/' + pavillon._id)
          .then(res => {
            store.commit('ADD_PATIENTS', res.data)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    addPatient: (store, patient) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/patients', patient)
          .then(res => {
            store.commit('ADD_PATIENT', res.data)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    removePatient: (store, id) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.delete('/patients/' + id)
          .then(res => {
            store.commit('REMOVE_PATIENT', id)
            resolve(res.data)
          })
          .catch(err => {
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    },
    updatePatient: (store, patient) => {
      MainStore.dispatch('loading', true)
      return new Promise((resolve, reject) => {
        axios.put('/patients/' + patient._id, patient)
          .then(res => {
            store.commit('UPDATE_PATIENT', patient)
            resolve(res.data)
          })
          .catch(err => {
            console.error(err)
            reject(err)
          })
          .then(_ => {
            MainStore.dispatch('loading', false)
          })
      })
    }
  }
}
