// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Toastr from 'vue-toastr'
// import vueEventCalendar from 'vue-event-calendar'
import 'jquery'

import App from './App'
import router from './router'
import store from '@/store/Store'

import 'vue-toastr/src/vue-toastr.scss'
import '@/assets/semantic/dist/semantic.min.css'
import '@/assets/semantic/dist/semantic.min.js'
// import 'vue-event-calendar/dist/style.css'

Vue.use(Toastr)
Vue.component('vue-toastr', Toastr)
// Vue.use(vueEventCalendar, { locale: 'fr' })

Vue.config.productionTip = false

Vue.filter('capitalize', (value) => {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})
Vue.filter('uncapitalize', (value) => {
  if (!value) return ''
  value = value.toString()
  return value.toLowerCase()
})
Vue.filter('computedRole', (role) => {
  if (!role) return ''
  role = role.toString()
  switch (role) {
    case 'NURSE':
      return 'Infi.' // TODO need translate
    case 'DIETITIAN':
      return 'Diététicien' // TODO need translate
    case 'DIETITIAN_CHIEF':
      return 'Diététicien en chef' // TODO need translate
    case 'RESPONSIBLE':
      return 'Responsable budget' // TODO need translate
    case 'ADMIN':
      return 'Admin' // TODO need translate
  }
  return 'UNKNOWN_ROLE'
})
Vue.filter('computedPeriod', (period) => {
  if (!period) return ''
  period = period.toString()
  switch (period) {
    case 'LUNCH':
      return 'Dîner' // TODO need translate
    case 'DINNER':
      return 'Souper' // TODO need translate
  }
  return 'UNKNOWN_PERIOD'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
