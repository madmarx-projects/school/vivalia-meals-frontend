import Vue from 'vue'
import Router from 'vue-router'
import jwt from 'jsonwebtoken'

import credentials from '@/utils/credentials'

import Navbar from '@/components/partials/navbar/TheNavbar'
import Sidebar from '@/components/partials/sidebar/TheSidebar'

import Pavillons from '@/components/pavillons/Pavillons'
import Pavillon from '@/components/pavillons/Pavillon'
import Users from '@/components/users/Users'
import Patients from '@/components/patients/Patients'
import Diets from '@/components/diets/Diets'
import Meals from '@/components/meals/Meals'
import Menus from '@/components/menus/Menus'
// import Calendar from '@/components/calendar/Calendar'
import ListOfTheDay from '@/components/lotd/ListOfTheDay'
import HistoryMeals from '@/components/history/History'

import Login from '@/components/pages/Login'
import Logout from '@/components/pages/Logout'
import Home from '@/components/pages/Home'
import HomeSecondary from '@/components/pages/HomeSecondary'
import Admin from '@/components/pages/Admin'
import Page404 from '@/components/pages/Page404'

Vue.use(Router)

const defaultComponents = {
  navbar: Navbar,
  sidebar: Sidebar
}

/**
 * Router definition
 */
let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        ...defaultComponents,
        main: Home,
        secondary: HomeSecondary
      },
      meta: {
        requiresAuth: true
      }
    }, {
      path: '/lotd',
      name: 'lotd',
      components: {
        ...defaultComponents,
        main: ListOfTheDay
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN', 'NURSE']
      }
    }, {
      path: '/pavillons',
      name: 'pavillons',
      components: {
        ...defaultComponents,
        main: Pavillons
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN']
      }
    }, {
      path: '/pavillon',
      name: 'pavillon',
      components: {
        ...defaultComponents,
        main: Pavillon
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN', 'NURSE']
      }
    }, {
      path: '/users',
      name: 'users',
      components: {
        ...defaultComponents,
        main: Users
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN']
      }
    }, {
      path: '/patients',
      name: 'patients',
      components: {
        ...defaultComponents,
        main: Patients
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN', 'NURSE']
      }
    }, {
      path: '/diets',
      name: 'diets',
      components: {
        ...defaultComponents,
        main: Diets
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN', 'DIETITIAN_CHIEF']
      }
    }, {
      path: '/meals',
      name: 'meals',
      components: {
        ...defaultComponents,
        main: Meals
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']
      }
    }, {
      path: '/menus',
      name: 'menus',
      components: {
        ...defaultComponents,
        main: Menus
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']
      }
    }, /* {
      path: '/calendar',
      name: 'calendar',
      components: {
        ...defaultComponents,
        main: Calendar
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']
      }
    }, */ {
      path: '/history',
      name: 'history',
      components: {
        ...defaultComponents,
        main: HistoryMeals
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN', 'RESPONSIBLE']
      }
    }, {
      path: '/admin',
      name: 'admin',
      components: {
        ...defaultComponents,
        main: Admin
      },
      meta: {
        requiresAuth: true,
        roles: ['ADMIN']
      }
    }, {
      path: '/login',
      name: 'login',
      components: {
        main: Login
      }
    }, {
      path: '/logout',
      name: 'logout',
      components: {
        main: Logout
      }
    }, {
      path: '*',
      name: '404',
      components: {
        ...defaultComponents,
        main: Page404
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('jwt') == null) {
      next({ name: 'login' })
    } else {
      if (to.matched.some(record => record.meta.roles)) {
        jwt.verify(localStorage.getItem('jwt'), credentials.secret, (err, decoded) => {
          if (err) {
            next({ name: 'login' })
          }

          let user = JSON.parse(localStorage.getItem('user'))
          if (user && to.meta.roles.indexOf(user.role) > -1) {
            next()
          } else {
            next({ name: 'home' })
          }
        })
      } else {
        next()
      }
    }
  } else {
    next()
  }
  // next()
})

export default router
