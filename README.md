# VivaliaMeals Documentation

## Description

VivaliaMeals est un logiciel a destination de l'établissement hospitalier
"La Clairière", situé à Bertrix (Province de Luxembourg, Belgique).  
Son but est de faciliter la commandes des repas au sein de l'hôpital.  
Les infirmières ont la possibilité de passer des commandes pour les patients
en fonction de leurs régimes spécifiques. Les diététiciens quant à eux ont la
possiblité de créer des menus à proposer et de gérer le calendrier.

## Project

### Description

Ce projet est la partie client du système.  
Vers la partie [backend](https://gitlab.com/Madmarx/vivalia-meals-api)

### Technos

- `vuejs` framework javascript pour le frontend [link](https://vuejs.org/)
- `vue-router` module vuejs pour la gestion des routes
[link](https://router.vuejs.org/)
- `vuex` module vuejs pour la gestion des states
[link](https://vuejs.org/v2/api/)
- `semantic-ui` framework css (équivalent de Bootstrap)
[link](https://semantic-ui.com/)


### Outils conseillés

- L'extension navigateur `Vue.js devtools` qui donne un contrôle sur le state
de l'application
- Si dev sur Visual Studio Code: l'extension `Vetur`

### Lancer ce projet

Installer les dépendances:

``` shell
npm i
```

Lancer en environnement de dev.

``` shell
npm start
```

Lancer en environnement de prod.  

``` shell
npm run build
npm run prod
```

### Prodécure de lancement du projet complet (**important** car contraignant)

La réussite n'est pas garantie si pas fait dans cet ordre:

- 1 Lancer le serveur mongodb
- Si pas déjà fait, préremplir la db avec les scripts (voir section ci-dessous)
- 2 Lancer le serveur backend
- 3 Accéder une première fois manuellement au backend
(typiquement: `https://localhost:1338/api/users`) et ajouter une exception pour
le certificat de l'api. Sinon impossible d'appeler l'api car le certificat n'est
pas validé par une autorité.
- 4 Lancer le serveur frontend


### PREMIER lancement: préremplir la db

Lancer un script dans l'instance de mongo

``` shell
mongo <your_mongo_host> <script>
```

#### Ajouter un admin et des sideMenus


Ajout de l'admin

``` shell
mongo vivalia database/script_add_admin.js
```

Ajout des sideMenus

``` shell
mongo vivalia database/script_add_sideMenus.js
```

### Mot de passe admin

- username: `admin`
- password: `admin`

Hash in database: 64be344935e89e3c9a21a31be5d2621b57d0c26b
