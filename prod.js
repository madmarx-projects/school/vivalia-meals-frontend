const express = require('express')
const app = express()
const https = require('https')
const fs = require('fs')

app.use(express.static(__dirname + '/dist'))

app.use('/', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
})

app.get('*', function(req, res){
  res.sendFile(__dirname + '/dist/index.html')
})

var options = {
  cert: fs.readFileSync('./localhost.crt'),
  key: fs.readFileSync('./localhost.key')
}
/**
 * Create HTTP server.
 */
const server = https.createServer(options, app)

const port = /*process.env.PORT ||*/ 1337
server.listen(port)
console.log(`Our app is running on port ${port}`)